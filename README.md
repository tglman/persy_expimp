# Persy Export Import Tool
[![build status](https://gitlab.com/persy/persy_expimp/badges/master/build.svg)](https://gitlab.com/persy/persy_expimp/commits/master)

Persy one file storage export import library and command line.

## Persy vs Persy ExpImp versions

| ExpImp Version   |  Can Export from     |  Can Import to        | Can read export made with |
| :-----:    | :------:    | :--------------: | :-------:|
| 0.1 | Persy 0.5 | Persy 0.5| 0.1 |
| 0.2 | Persy 0.6 | Persy 0.6| 0.1, 0.2 |
| 0.3 | Persy 0.7 | Persy 0.7| 0.1, 0.2, 0.3 |
| 0.4 | Persy 0.8 | Persy 0.8| 0.1, 0.2, 0.3, 0.4 |
| 0.5 | Persy 0.9 | Persy 0.9| 0.1, 0.2, 0.3, 0.4, 0.5 |
| 0.6 | Persy 0.10 | Persy 0.10| 0.1, 0.2, 0.3, 0.4, 0.5, 0.6 |
| 0.7 | Persy 0.11 | Persy 0.11| 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7 |
| 0.8 | Persy 1.0 | Persy 1.0| 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8 |
| 0.9 | Persy 1.1 | Persy 1.1| 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9 |
| 1.0 | Persy 1.\* | Persy 1.\* | 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0 |

## Command line install

```sh
cargo install persy_expimp --all-features --bin persy_expimp
```
old versions (Example 0.1):

```sh
cargo install persy_expimp --all-features --bin persy_expimp --version "1.0"
```

## Command line Usage

Export
```sh
./persy_expimp -e file.persy export.json
```

Import
```sh
./persy_expimp -i file.persy to_import.json

```

### Command line supported formats

JSON and bincode


## API Example


Export
```rust
use persy::{Persy, Config};
use persy_expimp::export;
use std::vec::Vec;
let persy = Persy::open("file.persy", Config::new())?;
for info in export(&persy)? {
    // Custom logic
}
```

Import
```rust
use persy::{Persy, Config};
use persy_expimp::{import,Info};
Persy::create("imported.persy")?;
let persy = Persy::open("imported.persy", Config::new())?;
// Source informations from a custom source
let source = Vec::<Info>::new();
import(&persy,source.into_iter())?;

```
