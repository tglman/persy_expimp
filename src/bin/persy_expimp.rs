use persy::{Config, Persy};
use persy_expimp::{export_binary, export_json, import_binary, import_json, Error};
use std::fs::OpenOptions;
use std::io::{stdin, stdout, Read, Write};
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(
    name = "persy_expimp",
    author = "tglman",
    about = "export or import data from a persy storage"
)]
struct Cli {
    #[structopt(long, short, default_value = "json")]
    format: String,

    #[structopt(long, short)]
    import: bool,

    #[structopt(long, short)]
    export: bool,

    #[structopt(parse(from_os_str))]
    persy_file: PathBuf,

    #[structopt(parse(from_os_str))]
    file: Option<PathBuf>,
}

fn format_export(format: &str, persy: Persy, writer: &mut dyn Write) -> Result<(), Error> {
    match format {
        "JSON" => export_json(&persy, writer)?,
        "BINCODE" => export_binary(&persy, writer)?,
        _ => panic!("supported formats are JSON and bincode format"),
    }
    Ok(())
}

fn format_import(format: &str, persy: Persy, reader: &mut dyn Read) -> Result<(), Error> {
    match format {
        "JSON" => import_json(&persy, reader)?,
        "BINCODE" => import_binary(&persy, reader)?,
        _ => panic!("supported formats are JSON and bincode format"),
    }
    Ok(())
}

pub fn main() -> Result<(), Error> {
    let cli = Cli::from_args();
    let format = cli.format.to_uppercase();
    if format != "JSON" && format != "BINCODE" {
        panic!("supported formats are only JSON and bincode format");
    }

    if cli.export {
        let persy = Persy::open(cli.persy_file.to_str().unwrap(), Config::new())?;
        if let Some(f) = cli.file {
            let mut output = OpenOptions::new().write(true).create(true).open(f)?;
            format_export(&format, persy, &mut output)?;
        } else {
            format_export(&format, persy, &mut stdout())?;
        }
    } else if cli.import {
        Persy::create(cli.persy_file.to_str().unwrap())?;
        let persy = Persy::open(cli.persy_file.to_str().unwrap(), Config::new())?;
        if let Some(f) = cli.file {
            let mut input = OpenOptions::new().read(true).open(f)?;
            format_import(&format, persy, &mut input)?;
        } else {
            format_import(&format, persy, &mut stdin())?;
        }
    } else {
        println!("please specify if export or import");
    }

    Ok(())
}
