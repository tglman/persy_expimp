use std::ops::Deref;

use persy::{ByteVec, IndexId, IndexTypeId, PersyId, SegmentId, ValueMode};
use serde::{Deserialize, Deserializer, Serializer};
use serde_derive::{Deserialize, Serialize};

pub(crate) fn index_type_id_to_string(id: IndexTypeId) -> String {
    match id {
        IndexTypeId::U8 => "U8".to_string(),
        IndexTypeId::U16 => "U16".to_string(),
        IndexTypeId::U32 => "U32".to_string(),
        IndexTypeId::U64 => "U64".to_string(),
        IndexTypeId::U128 => "U128".to_string(),
        IndexTypeId::I8 => "I8".to_string(),
        IndexTypeId::I16 => "I16".to_string(),
        IndexTypeId::I32 => "I32".to_string(),
        IndexTypeId::I64 => "I64".to_string(),
        IndexTypeId::I128 => "I128".to_string(),
        IndexTypeId::F32W => "F32".to_string(),
        IndexTypeId::F64W => "F64".to_string(),
        IndexTypeId::String => "String".to_string(),
        IndexTypeId::PersyId => "PersyId".to_string(),
        IndexTypeId::ByteVec => "ByteVec".to_string(),
    }
}

pub(crate) fn index_type_id_from_string(id: &str) -> Option<IndexTypeId> {
    match id {
        "U8" => Some(IndexTypeId::U8),
        "U16" => Some(IndexTypeId::U16),
        "U32" => Some(IndexTypeId::U32),
        "U64" => Some(IndexTypeId::U64),
        "U128" => Some(IndexTypeId::U128),
        "I8" => Some(IndexTypeId::I8),
        "I16" => Some(IndexTypeId::I16),
        "I32" => Some(IndexTypeId::I32),
        "I64" => Some(IndexTypeId::I64),
        "I128" => Some(IndexTypeId::I128),
        "F32" => Some(IndexTypeId::F32W),
        "F64" => Some(IndexTypeId::F64W),
        "String" => Some(IndexTypeId::String),
        "PersyId" => Some(IndexTypeId::PersyId),
        "ByteVec" => Some(IndexTypeId::ByteVec),
        _ => None,
    }
}
pub fn segment_id_serialize<S: Serializer>(value: &SegmentId, serilizer: S) -> Result<S::Ok, S::Error> {
    serilizer.serialize_str(&value.to_string())
}
pub fn segment_id_deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<SegmentId, D::Error> {
    let value = String::deserialize(deserializer)?;
    value.parse::<SegmentId>().map_err(serde::de::Error::custom)
}

pub fn persy_id_serialize<S: Serializer>(value: &PersyId, serilizer: S) -> Result<S::Ok, S::Error> {
    serilizer.serialize_str(&value.to_string())
}

pub fn persy_id_deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<PersyId, D::Error> {
    let value = String::deserialize(deserializer)?;
    value.parse::<PersyId>().map_err(serde::de::Error::custom)
}

#[derive(Serialize, Deserialize)]
struct PersyIdWrapper(
    #[serde(serialize_with = "persy_id_serialize", deserialize_with = "persy_id_deserialize")] PersyId,
);

pub fn vec_persy_id_serialize<S: Serializer>(value: &Vec<PersyId>, serilizer: S) -> Result<S::Ok, S::Error> {
    let new_val = value
        .into_iter()
        .map(|val| PersyIdWrapper(val.clone()))
        .collect::<Vec<_>>();
    serde::ser::Serialize::serialize(&new_val, serilizer)
}

pub fn vec_persy_id_deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<Vec<PersyId>, D::Error> {
    let value = Vec::deserialize(deserializer)?;
    Ok(value.into_iter().map(|PersyIdWrapper(val)| val).collect())
}

pub fn bytevec_serialize<S: Serializer>(value: &ByteVec, serilizer: S) -> Result<S::Ok, S::Error> {
    serde::ser::Serialize::serialize(value.deref(), serilizer)
}

pub fn bytevec_deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<ByteVec, D::Error> {
    let value = Vec::<u8>::deserialize(deserializer)?;
    Ok(ByteVec::new(value))
}

pub fn vec_bytevec_serialize<S: Serializer>(value: &Vec<ByteVec>, serilizer: S) -> Result<S::Ok, S::Error> {
    let new_val = value
        .into_iter()
        .map(|val| val.deref().to_owned())
        .collect::<Vec<Vec<u8>>>();
    serde::ser::Serialize::serialize(&new_val, serilizer)
}

pub fn vec_bytevec_deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<Vec<ByteVec>, D::Error> {
    let value = Vec::<Vec<u8>>::deserialize(deserializer)?;
    Ok(value.into_iter().map(|val| ByteVec::new(val)).collect())
}

pub fn index_id_serialize<S: Serializer>(value: &IndexId, serilizer: S) -> Result<S::Ok, S::Error> {
    serilizer.serialize_str(&value.to_string())
}

pub fn index_id_deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<IndexId, D::Error> {
    let value = String::deserialize(deserializer)?;
    value.parse::<IndexId>().map_err(serde::de::Error::custom)
}

pub fn index_type_id_serialize<S: Serializer>(value: &IndexTypeId, serilizer: S) -> Result<S::Ok, S::Error> {
    serilizer.serialize_str(&index_type_id_to_string(value.clone()))
}

pub fn index_type_id_deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<IndexTypeId, D::Error> {
    let value = String::deserialize(deserializer)?;
    index_type_id_from_string(&value)
        .ok_or("Invalid Type Id")
        .map_err(serde::de::Error::custom)
}

pub fn value_mode_serialize<S: Serializer>(value: &ValueMode, serilizer: S) -> Result<S::Ok, S::Error> {
    let mode = match value {
        ValueMode::Replace => "replace",
        ValueMode::Cluster => "cluster",
        ValueMode::Exclusive => "exclusive",
    };
    serilizer.serialize_str(mode)
}

pub fn value_mode_deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<ValueMode, D::Error> {
    let value = String::deserialize(deserializer)?;
    let mode = match value.as_str() {
        "replace" => ValueMode::Replace,
        "cluster" => ValueMode::Cluster,
        "exclusive" => ValueMode::Exclusive,
        _ => return Err(format!("Value Mode '{}' does not exists", value)).map_err(serde::de::Error::custom),
    };
    Ok(mode)
}
